# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [1.0.0] 17-03-2021

### Added

- Introduced **respec-config.js** to provide custom ResPec-configuration above **Markdow** parsing
- Parsing the Header of the SUMMARY as document title
- Pre Processing Kroki.io Apis call's

## [Unreleased]

### Added

- Initial setup Typescript development CLI (commandline shell) and Lib (library rollup)
