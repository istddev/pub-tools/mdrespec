// All config options at https://respec.org/docs/
var respecConfig = {
    specStatus: 'unofficial',
    editors: [
      { name: 'René Hietkamp', url: 'https://kik-v.nl' },
      { name: 'Onno Haldar', url: 'https://kik-v.nl' }
    ],
    github: 'onnohaldar/respec-kik-v',
    maxTocLevel: 3,
};
