# Contributing

## Designers / Developers

- ![1](https://assets.gitlab-static.net/uploads/-/system/user/avatar/6216723/avatar.png?width=40) [Rene Hietkamp](https://gitlab.com/hietkamp)

- ![2](https://assets.gitlab-static.net/uploads/-/system/user/avatar/8167154/avatar.png?width=40) [Onno Haldar](https://gitlab.com/onnohaldar)

## Reviewers / Testers

- pm

[![Ecosysteem: NPM](https://img.shields.io/badge/NPM-Ecosystem-brightgreen)](https://www.npmjs.com/)
