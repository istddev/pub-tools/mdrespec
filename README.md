# Pub Tools / MdResPec [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](LICENSE)

Tools to help parse _Mark Down_ and other __ResPec-template__ reuse helpers. Based on a ResPec builded templates [see ResPec developers guide](https://github.com/w3c/respec/wiki/Developers-Guide).

## How doest it work

```plantuml
@startuml@startuml
start
:Read **SUMMARY** Header as title and lines as **Markdown** references;
:Parse **Markdown** references into ResPec **index** body;
:Pre-process **Markdown** files for *ResPec* requirements and *Kroki.io* diagram Api;
:Write **ResPec publication** with parsed and pre-processed content;
stop
@enduml
```

## NPM installation

````shell
npm install https://gitlab.com/istddev/pub-tools/mdrespec.git --save
````

## MdResPec CLI

CLI to parse Content into Markdown-link based ResPec-templates.

### Pre-conditions Markdown Content

The next _Mark Down_ files should be available in the _input folder_:

- a ```respec-config.js``` with a ```var respecConfig = {...}``` for all optional ResPec custom configuration setings. [See ResPec Documentation](https://respec.org/docs/) and [example in this repo](./respec-config.js)

- a ```SUMMARY.md``` file with _GitBook_ like content but with only one level and starting with "[Abstract]" and ending with "[Conformance]" (required by ResPec). *Note: The Header will be used as document title.*

*See example ```SUMMARY.md``` content*
```markdown
# Summary

* [Abstract](ABSTRACT.md)
* [Readme](README.md)
* [Changelog](CHANGELOG.md)
* [Contributing](CONTRIBUTING.md)
* [Conformance](CONFORMANCE.md)
```
- a ```ABSTRACT.md``` file (referenced in SUMMARY) with _ResPec_ required abstract text

- a ```CONFORMANCE.md``` file (referenced in SUMMARY) with _ResPec_ required conformance text

### Pre-conditions ResPec Template

You can use the default provided template or create a custom from a ResPec-profile.

If you do so then the ResPec Template should contain
  
+- __index.html.template__ = _required_  
|  
+- assets/ = _not required (template-assets)_  
|  
+- __respec/__ = _required ResPec Profile (.js and .map)_

The required __index.html.template__ must contain the following HTML-coding:

- a HTML-lang tag with the *language-code* (nl, fr, ..) of the document content

- in the Head-section a Title-section with the placeholder **<=% docTitle %>** where the CLI will parse the *Header* of the *SUMMARY* as the document title)

- in the Head-section a Script-section with refrence to the used *ResPec-profile*

```html
<script src="./respec/(ResPec-profile).js" async class="remove"></script>
```

- in the Head-section a Script-section with reference to the *custom-settings* and the required *markdown-format setting*

```html
<script src="./respec-config.js" class="remove">
    respecConfig.format = 'markdown'; 
</script>
```

- a Body-section with the placeholder **<=% mdSections %>** where the CLI will parse the sections to include the Markdown file content

See also provided default template [index.html.template](https://gitlab.com/istddev/pub-tools/mdrespec/-/blob/master/template/index.html.template)

### Command Line

Run CLI using NPX:

```bash
npx mdrespec contentPath publishPath templatePath
```

Or as a script in "package.json":

```npm
{
  ...
  "scripts": {
    ...
    "build:respec": "mdrespec contentPath publishPath templatePath"
    ...
  }
  ...
}
```

| Parm | Description | Required |
|-|:-|:-:|
| contentPath | Markdown content directory | Y |
| publishPath | ResPec publication path | Y |
| templatePath | Custom ResPec HTML template directory (alternative for default supplied template) | N |

