/**
 * ResPec Utils
 */

/**
 * Node Package Imports
 */
import { readFileSync, writeFileSync, existsSync } from 'fs';
import { join, resolve } from 'path';
import { copySync } from 'cpx';

import { resolveTemplatePath, createTemplate, extractSectionId, extractDataInclude } from '@pub-tools/mdlib-ts';
// Open Source van eigen maak (n.t.b. onderbrengen waar?)
import { preProcessKrokiMdContent, writePreProcessedDestFile } from '@dgwnu/md-pre-kroki'; 

/**
 * Constant Values
 */
const SUMMARY_MD = 'SUMMARY.md';
const INDEX_HTML_TEMPLATE = 'index.html.template';
const RESPEC_CONFIG_JS = 'respec-config.js';

/**
 * Parse Marddow files based on (GitBook like) SUMMARY-file and default (or specific provided) Template
 * @param mdContentPath 
 * @param resPecPublishPath 
 * @param resPecTemplatePath 
 */
export function parseMd2ResPec(mdContentPath: string, resPecPublishPath: string, resPecTemplatePath?: string) {
    resPecTemplatePath = resolveTemplatePath(__dirname, resPecTemplatePath);
    mdContentPath = resolve(mdContentPath);
    console.log(`mdContentPath = ${mdContentPath}`);
    resPecPublishPath = resolve(resPecPublishPath);
    console.log(`resPecPublishPath = ${resPecPublishPath}`);
    console.log(`resPecTemplatePath = ${resPecTemplatePath}`);

    // check for mandatory respecConfig
    const respecConfigFilePath = join(mdContentPath, RESPEC_CONFIG_JS);
    if (!existsSync(respecConfigFilePath)) {
        throw new Error(`ResPecMd CLI - Missing required "${RESPEC_CONFIG_JS}" file!`);
    }
    // check for mandatory SUMMARY
    const summaryFilePath = join(mdContentPath, SUMMARY_MD);
    if (!existsSync(summaryFilePath)) {
        throw new Error(`ResPecMd CLI - Missing required "${SUMMARY_MD}" file!`);
    }

    // init ResPec output with template-profile and assets 
    copySync(`${resPecTemplatePath}/**/respec/**`, resPecPublishPath);
    copySync(`${resPecTemplatePath}/**/assets/**`, resPecPublishPath);
    // copy respecConfig to output
    copySync(respecConfigFilePath, resPecPublishPath);

    // read md content summary
    const summaryMdStr = readFileSync(summaryFilePath, 'utf-8');
    // get summary header as document title
    const docTitle =  summaryMdStr.split('\n')[0].substring(1).trim();
    // initialize list with summary lines (all lines that start with * or spaces and *)
    const summaryLines = summaryMdStr.split('\n').filter(mdLine => mdLine.trimStart().startsWith('*'));

    // check ResPec first section requirements
    const abstractLineNr = summaryLines.findIndex(mdLine => extractSectionId(mdLine) == 'abstract');
    if (abstractLineNr != 0) {
        throw new Error('ResPecMd CLI - SUMMARY.md must start with a "* [Abstract](file path to a <ABSTRACT>.md)" Section!');
    }
    // check ResPec last section requirements
    const conformanceLineNr = summaryLines.findIndex(mdLine => extractSectionId(mdLine) == 'conformance');
    if (conformanceLineNr != summaryLines.length - 1) {
        throw new Error('ResPecMd CLI - SUMMARY.md must end with a "* [Conformance](file path to a <CONFORMANCE>.md)" Section!');
    }

    // assemble (sub)sections from summary
    // and process ResPec and Kroki.io Api requirements for Mark Down content
    let mdSections = '';
    let summaryLineNr = 0;

    for (const summaryLine of summaryLines) {
        // extract data from content
        const sectionId = extractSectionId(summaryLine);
        const dataInclude = extractDataInclude(summaryLine);
        // generate new HTML section
        mdSections += parseSection(sectionId, dataInclude);
        // read Mark Down Content from input directory
        const mdContentFilePath = join(mdContentPath, dataInclude);
        let mdContentStr = readFileSync(mdContentFilePath, 'utf-8');

        if (summaryLineNr == 0 || summaryLineNr == summaryLines.length - 1) {
            // Abstract and Conformance Mark Dow must be without Headers (already supplied by ResPec)
            mdContentStr = removeMdHeader(mdContentStr);
        }

        // write Mark Down Content to output directory (including pre processing Kroki.io Apis call's)
        writePreProcessedDestFile(mdContentPath, resPecPublishPath, mdContentFilePath, preProcessKrokiMdContent(mdContentStr));
        // next summary line to process
        summaryLineNr ++;
    }
    
    // write parsed content in template to output directory
    const htmlIndexTemplate = readFileSync(join(resPecTemplatePath, INDEX_HTML_TEMPLATE), 'utf-8');
    writeFileSync(
        join(resPecPublishPath, 'index.html'), 
        htmlIndexTemplate
            .replace(createTemplate('docTitle'), docTitle)
            .replace(createTemplate('mdSections'), mdSections)
    );
}

/**
 * Markdown-file with (or without) Top Header
 * @param mdFileStr 
 * @returns Markdown-file without Top Header
 */
function removeMdHeader(mdFileStr: string) {
    return mdFileStr.split('\n')
        .filter(mdLine => !mdLine.startsWith('# '))
            .join('\n');
}

/**
 * <section id="inleiding" data-format="markdown" data-include="README.md"><h2></h2></section>
 * @param sectionLevel 
 * @param sectionId 
 * @param dataInclude 
 */
function parseSection(sectionId: string, dataInclude: string) {
    // Parse Mark Down Values in HTML Template String

    if (sectionId == 'abstract' || sectionId == 'conformance') {
        return `<section id="${sectionId}"><div data-format="markdown" data-include="${dataInclude}"></div></section>\n`;
    } else {
        return `<section data-format="markdown" data-include="${dataInclude}"><h2></h2></section>\n`;
    }

}
