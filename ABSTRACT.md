# Abstract

Deze documentatie beschrijft het gebruik van publicatie tooling voor __ResPec-publicaties__ vanuit __Markdown__ bronnen. Voorlopige doel is het verkennen en evalueren van de mogelijke opties voor het publiceren van technische specificaties voor de KIK-V standaard.

Zie ook repostitory [iStdDev > Pub Tools > mdrespec](https://gitlab.com/istddev/pub-tools/mdrespec).
