import { existsSync, readFileSync, writeFileSync } from 'fs';
import { resolve, join } from 'path';
import { copySync } from 'cpx';
import { resolveTemplatePath, extractSectionId, extractDataInclude, createTemplate } from '@pub-tools/mdlib-ts';
import { writePreProcessedDestFile, preProcessKrokiMdContent } from '@dgwnu/md-pre-kroki';

/**
 * ResPec Utils
 */
/**
 * Constant Values
 */
var SUMMARY_MD = 'SUMMARY.md';
var INDEX_HTML_TEMPLATE = 'index.html.template';
var RESPEC_CONFIG_JS = 'respec-config.js';
/**
 * Parse Marddow files based on (GitBook like) SUMMARY-file and default (or specific provided) Template
 * @param mdContentPath
 * @param resPecPublishPath
 * @param resPecTemplatePath
 */
function parseMd2ResPec(mdContentPath, resPecPublishPath, resPecTemplatePath) {
    resPecTemplatePath = resolveTemplatePath(__dirname, resPecTemplatePath);
    mdContentPath = resolve(mdContentPath);
    console.log("mdContentPath = " + mdContentPath);
    resPecPublishPath = resolve(resPecPublishPath);
    console.log("resPecPublishPath = " + resPecPublishPath);
    console.log("resPecTemplatePath = " + resPecTemplatePath);
    // check for mandatory respecConfig
    var respecConfigFilePath = join(mdContentPath, RESPEC_CONFIG_JS);
    if (!existsSync(respecConfigFilePath)) {
        throw new Error("ResPecMd CLI - Missing required \"" + RESPEC_CONFIG_JS + "\" file!");
    }
    // check for mandatory SUMMARY
    var summaryFilePath = join(mdContentPath, SUMMARY_MD);
    if (!existsSync(summaryFilePath)) {
        throw new Error("ResPecMd CLI - Missing required \"" + SUMMARY_MD + "\" file!");
    }
    // init ResPec output with template-profile and assets 
    copySync(resPecTemplatePath + "/**/respec/**", resPecPublishPath);
    copySync(resPecTemplatePath + "/**/assets/**", resPecPublishPath);
    // copy respecConfig to output
    copySync(respecConfigFilePath, resPecPublishPath);
    // read md content summary
    var summaryMdStr = readFileSync(summaryFilePath, 'utf-8');
    // get summary header as document title
    var docTitle = summaryMdStr.split('\n')[0].substring(1).trim();
    // initialize list with summary lines (all lines that start with * or spaces and *)
    var summaryLines = summaryMdStr.split('\n').filter(function (mdLine) { return mdLine.trimStart().startsWith('*'); });
    // check ResPec first section requirements
    var abstractLineNr = summaryLines.findIndex(function (mdLine) { return extractSectionId(mdLine) == 'abstract'; });
    if (abstractLineNr != 0) {
        throw new Error('ResPecMd CLI - SUMMARY.md must start with a "* [Abstract](file path to a <ABSTRACT>.md)" Section!');
    }
    // check ResPec last section requirements
    var conformanceLineNr = summaryLines.findIndex(function (mdLine) { return extractSectionId(mdLine) == 'conformance'; });
    if (conformanceLineNr != summaryLines.length - 1) {
        throw new Error('ResPecMd CLI - SUMMARY.md must end with a "* [Conformance](file path to a <CONFORMANCE>.md)" Section!');
    }
    // assemble (sub)sections from summary
    // and process ResPec and Kroki.io Api requirements for Mark Down content
    var mdSections = '';
    var summaryLineNr = 0;
    for (var _i = 0, summaryLines_1 = summaryLines; _i < summaryLines_1.length; _i++) {
        var summaryLine = summaryLines_1[_i];
        // extract data from content
        var sectionId = extractSectionId(summaryLine);
        var dataInclude = extractDataInclude(summaryLine);
        // generate new HTML section
        mdSections += parseSection(sectionId, dataInclude);
        // read Mark Down Content from input directory
        var mdContentFilePath = join(mdContentPath, dataInclude);
        var mdContentStr = readFileSync(mdContentFilePath, 'utf-8');
        if (summaryLineNr == 0 || summaryLineNr == summaryLines.length - 1) {
            // Abstract and Conformance Mark Dow must be without Headers (already supplied by ResPec)
            mdContentStr = removeMdHeader(mdContentStr);
        }
        // write Mark Down Content to output directory (including pre processing Kroki.io Apis call's)
        writePreProcessedDestFile(mdContentPath, resPecPublishPath, mdContentFilePath, preProcessKrokiMdContent(mdContentStr));
        // next summary line to process
        summaryLineNr++;
    }
    // write parsed content in template to output directory
    var htmlIndexTemplate = readFileSync(join(resPecTemplatePath, INDEX_HTML_TEMPLATE), 'utf-8');
    writeFileSync(join(resPecPublishPath, 'index.html'), htmlIndexTemplate
        .replace(createTemplate('docTitle'), docTitle)
        .replace(createTemplate('mdSections'), mdSections));
}
/**
 * Markdown-file with (or without) Top Header
 * @param mdFileStr
 * @returns Markdown-file without Top Header
 */
function removeMdHeader(mdFileStr) {
    return mdFileStr.split('\n')
        .filter(function (mdLine) { return !mdLine.startsWith('# '); })
        .join('\n');
}
/**
 * <section id="inleiding" data-format="markdown" data-include="README.md"><h2></h2></section>
 * @param sectionLevel
 * @param sectionId
 * @param dataInclude
 */
function parseSection(sectionId, dataInclude) {
    // Parse Mark Down Values in HTML Template String
    if (sectionId == 'abstract' || sectionId == 'conformance') {
        return "<section id=\"" + sectionId + "\"><div data-format=\"markdown\" data-include=\"" + dataInclude + "\"></div></section>\n";
    }
    else {
        return "<section data-format=\"markdown\" data-include=\"" + dataInclude + "\"><h2></h2></section>\n";
    }
}

export { parseMd2ResPec };
