/**
 * ResPec Utils
 */
/**
 * Parse Marddow files based on (GitBook like) SUMMARY-file and default (or specific provided) Template
 * @param mdContentPath
 * @param resPecPublishPath
 * @param resPecTemplatePath
 */
export declare function parseMd2ResPec(mdContentPath: string, resPecPublishPath: string, resPecTemplatePath?: string): void;
