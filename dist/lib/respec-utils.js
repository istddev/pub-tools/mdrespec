"use strict";
/**
 * ResPec Utils
 */
Object.defineProperty(exports, "__esModule", { value: true });
exports.parseMd2ResPec = void 0;
/**
 * Node Package Imports
 */
const fs_1 = require("fs");
const path_1 = require("path");
const cpx_1 = require("cpx");
const mdlib_ts_1 = require("@pub-tools/mdlib-ts");
// Open Source van eigen maak (n.t.b. onderbrengen waar?)
const md_pre_kroki_1 = require("@dgwnu/md-pre-kroki");
/**
 * Constant Values
 */
const SUMMARY_MD = 'SUMMARY.md';
const INDEX_HTML_TEMPLATE = 'index.html.template';
const RESPEC_CONFIG_JS = 'respec-config.js';
/**
 * Parse Marddow files based on (GitBook like) SUMMARY-file and default (or specific provided) Template
 * @param mdContentPath
 * @param resPecPublishPath
 * @param resPecTemplatePath
 */
function parseMd2ResPec(mdContentPath, resPecPublishPath, resPecTemplatePath) {
    resPecTemplatePath = mdlib_ts_1.resolveTemplatePath(__dirname, resPecTemplatePath);
    mdContentPath = path_1.resolve(mdContentPath);
    console.log(`mdContentPath = ${mdContentPath}`);
    resPecPublishPath = path_1.resolve(resPecPublishPath);
    console.log(`resPecPublishPath = ${resPecPublishPath}`);
    console.log(`resPecTemplatePath = ${resPecTemplatePath}`);
    // check for mandatory respecConfig
    const respecConfigFilePath = path_1.join(mdContentPath, RESPEC_CONFIG_JS);
    if (!fs_1.existsSync(respecConfigFilePath)) {
        throw new Error(`ResPecMd CLI - Missing required "${RESPEC_CONFIG_JS}" file!`);
    }
    // check for mandatory SUMMARY
    const summaryFilePath = path_1.join(mdContentPath, SUMMARY_MD);
    if (!fs_1.existsSync(summaryFilePath)) {
        throw new Error(`ResPecMd CLI - Missing required "${SUMMARY_MD}" file!`);
    }
    // init ResPec output with template-profile and assets 
    cpx_1.copySync(`${resPecTemplatePath}/**/respec/**`, resPecPublishPath);
    cpx_1.copySync(`${resPecTemplatePath}/**/assets/**`, resPecPublishPath);
    // copy respecConfig to output
    cpx_1.copySync(respecConfigFilePath, resPecPublishPath);
    // read md content summary
    const summaryMdStr = fs_1.readFileSync(summaryFilePath, 'utf-8');
    // get summary header as document title
    const docTitle = summaryMdStr.split('\n')[0].substring(1).trim();
    // initialize list with summary lines (all lines that start with * or spaces and *)
    const summaryLines = summaryMdStr.split('\n').filter(mdLine => mdLine.trimStart().startsWith('*'));
    // check ResPec first section requirements
    const abstractLineNr = summaryLines.findIndex(mdLine => mdlib_ts_1.extractSectionId(mdLine) == 'abstract');
    if (abstractLineNr != 0) {
        throw new Error('ResPecMd CLI - SUMMARY.md must start with a "* [Abstract](file path to a <ABSTRACT>.md)" Section!');
    }
    // check ResPec last section requirements
    const conformanceLineNr = summaryLines.findIndex(mdLine => mdlib_ts_1.extractSectionId(mdLine) == 'conformance');
    if (conformanceLineNr != summaryLines.length - 1) {
        throw new Error('ResPecMd CLI - SUMMARY.md must end with a "* [Conformance](file path to a <CONFORMANCE>.md)" Section!');
    }
    // assemble (sub)sections from summary
    // and process ResPec and Kroki.io Api requirements for Mark Down content
    let mdSections = '';
    let summaryLineNr = 0;
    for (const summaryLine of summaryLines) {
        // extract data from content
        const sectionId = mdlib_ts_1.extractSectionId(summaryLine);
        const dataInclude = mdlib_ts_1.extractDataInclude(summaryLine);
        // generate new HTML section
        mdSections += parseSection(sectionId, dataInclude);
        // read Mark Down Content from input directory
        const mdContentFilePath = path_1.join(mdContentPath, dataInclude);
        let mdContentStr = fs_1.readFileSync(mdContentFilePath, 'utf-8');
        if (summaryLineNr == 0 || summaryLineNr == summaryLines.length - 1) {
            // Abstract and Conformance Mark Dow must be without Headers (already supplied by ResPec)
            mdContentStr = removeMdHeader(mdContentStr);
        }
        // write Mark Down Content to output directory (including pre processing Kroki.io Apis call's)
        md_pre_kroki_1.writePreProcessedDestFile(mdContentPath, resPecPublishPath, mdContentFilePath, md_pre_kroki_1.preProcessKrokiMdContent(mdContentStr));
        // next summary line to process
        summaryLineNr++;
    }
    // write parsed content in template to output directory
    const htmlIndexTemplate = fs_1.readFileSync(path_1.join(resPecTemplatePath, INDEX_HTML_TEMPLATE), 'utf-8');
    fs_1.writeFileSync(path_1.join(resPecPublishPath, 'index.html'), htmlIndexTemplate
        .replace(mdlib_ts_1.createTemplate('docTitle'), docTitle)
        .replace(mdlib_ts_1.createTemplate('mdSections'), mdSections));
}
exports.parseMd2ResPec = parseMd2ResPec;
/**
 * Markdown-file with (or without) Top Header
 * @param mdFileStr
 * @returns Markdown-file without Top Header
 */
function removeMdHeader(mdFileStr) {
    return mdFileStr.split('\n')
        .filter(mdLine => !mdLine.startsWith('# '))
        .join('\n');
}
/**
 * <section id="inleiding" data-format="markdown" data-include="README.md"><h2></h2></section>
 * @param sectionLevel
 * @param sectionId
 * @param dataInclude
 */
function parseSection(sectionId, dataInclude) {
    // Parse Mark Down Values in HTML Template String
    if (sectionId == 'abstract' || sectionId == 'conformance') {
        return `<section id="${sectionId}"><div data-format="markdown" data-include="${dataInclude}"></div></section>\n`;
    }
    else {
        return `<section data-format="markdown" data-include="${dataInclude}"><h2></h2></section>\n`;
    }
}
